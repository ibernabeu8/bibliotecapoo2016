package gestorPrestamos;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

import gestorMateriales.Material;
import utilidades.GeneradorURL;

public class GestorPrestamos {
	
	//PROPIEDADES
	private final String URL = "usr/prestamos.dat";
	///
	private HashSet<Prestamo> lista = new HashSet<Prestamo>();
	//
	//
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	
	public GestorPrestamos() {
		init();
	}
	
	/*lo primero es cargar los prestamos 
	 * */
	private void init() {
		GeneradorURL.crearRuta(URL);
		cargar();
	}
	
	//Nuevo prestamo
	public Prestamo nuevoPrestamo(String idSocio, Material m) {

		Date fechaPrestamo = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fechaPrestamo);
		calendar.add(Calendar.DAY_OF_YEAR, 5);// 5 dias

		Prestamo nuevoPrestamo = new Prestamo(m, fechaPrestamo, calendar.getTime(), idSocio);
		this.lista.add(nuevoPrestamo);
		guardar();
		return nuevoPrestamo;
	}
	
	//devolver prestamo
	public Material devolverPrestamo(String idPrestamo) {
		Iterator<Prestamo> it = this.lista.iterator();
		System.out.println("devolver?");
		while(it.hasNext()) {
			Prestamo p = it.next();
			
			if(p.getId().equals(idPrestamo)) {
				System.out.println("entra?");
				this.lista.remove(p);
				guardar();
				return p.getMat();
			}
		}
		return null;
	}
	public HashSet<Prestamo> getLista() {
		return this.lista;
	}
	//
	private void cargar() {
		try {
			this.lista = new HashSet<Prestamo>();
			this.ois = new ObjectInputStream(new FileInputStream(this.URL));
			this.lista = (HashSet<Prestamo>) this.ois.readObject();
			this.ois.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (EOFException e) {
			System.out.println("Final de archivo.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	//
	private void guardar() {
		try{
			this.oos = new ObjectOutputStream(new FileOutputStream(this.URL));
			this.oos.writeObject(this.lista);
			this.oos.close();
		}catch (EOFException e) {
			System.out.println("Final de archivo.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
