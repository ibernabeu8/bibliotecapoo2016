package gestorPrestamos;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import gestorMateriales.Audio;
import gestorMateriales.Libro;
import gestorMateriales.Material;
import gestorMateriales.Periodico;
import gestorMateriales.Revista;
import gestorMateriales.Video;

public class Prestamo implements Serializable{
	
	//PROPIEDADES
	private String idSocio;
	private Material mat;
	private Date fechaInicio,fechaDevolucion;
    private String id;
    
    public Prestamo(){}
    
    public Prestamo(String id) {}
    
    public Prestamo(Material m, Date fechaInicio, Date fechaDevolucion, String idSocio) {
    	this.id = UUID.randomUUID().toString();
    	this.mat = m;
    	this.fechaInicio = fechaInicio;
    	this.fechaDevolucion = fechaDevolucion;
    	this.idSocio = idSocio;
    }
    //GETS && SETS
	public Date getFechaInicio() {
		return fechaInicio;
	}

	public Date getFechaDevolucion() {
		return fechaDevolucion;
	}

	public String getId() {
		return id;
	}

	public Material getMat() {
		return mat;
	}
	public String getIdSocio() {
		return idSocio;
	}

	public void setIdSocio(String idSocio) {
		this.idSocio = idSocio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public void setFechaDevolucion(Date fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setMat(Material mat) {
		this.mat = mat;
	}
    //metodos

	@Override
	public String toString() {
		String tipo = "";
		if(mat instanceof Libro) {
			tipo = "libro";
		}else if(mat instanceof Revista){
			tipo = "revista";
		}else if(mat instanceof Periodico) {
			tipo = "periodico";
		}else if(mat instanceof Audio) {
			tipo = "audio";
		}else if(mat instanceof Video) {
			tipo = "video";
		}
		return "Prestamo, tipo : " + tipo + ", fechaInicio: " + fechaInicio + ", fechaDevolucion: " + fechaDevolucion
				+ " COD: " + id + " CodSocio: " + idSocio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Prestamo))
			return false;
		Prestamo other = (Prestamo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
