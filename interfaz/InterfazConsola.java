package interfaz;

import java.util.Scanner;

import controlador.SGIB;
import gestorMateriales.Audio;
import gestorMateriales.Libro;
import gestorMateriales.Periodico;
import gestorMateriales.Revista;
import gestorMateriales.Video;
import gestorPrestamos.Prestamo;
import gestorUsuarios.Bibliotecario;
import gestorUsuarios.Socio;
import gestorUsuarios.Usuario;
import utilidades.LecturaConsola;

public class InterfazConsola extends SIGBInterfaz {
	
	//campos
	private Scanner in = new Scanner(System.in);
	private Socio s = null;
	private Bibliotecario b = null;
	private boolean salir = false;
	private boolean menuBibliotecario;
	private boolean menuSocio;
	public static void main(String[] args) {
		InterfazConsola ic = new InterfazConsola();
		ic.init();
	}
	/*
	 * El metodo init es llamado al iniciar la aplicación y se encarga de mostrar el primer menu segun el tipo de usuario
	 * que haga login en el sistema.
	 * (non-Javadoc)
	 * @see interfaz.SIGBInterfaz#init()
	 */
	@Override
	public void init() {
		
		this.sgib = new SGIB();
		
		System.out.println("=====================================================");
		System.out.println("Bienvenido al sistema bilbiotecario POO2016");
		System.out.println();
		//PARTE USUARIO
		Usuario u = login();
		if(u instanceof Bibliotecario) {
			this.b = (Bibliotecario)u;
			this.menuBibliotecario = true;
		}else if(u instanceof Socio) {
			this.s = (Socio)u;
			this.menuSocio = true;
		}
		//FIN
		//Menu bibliotecario
		if(menuBibliotecario) {
			do {
				System.out.println("1.Gestion usuarios");
				System.out.println("2.Gestion materiales");
				System.out.println("3.Gestion suscripciones");
				System.out.println("4.Gestion prestamos");
				System.out.println("5.Imprimir tarjeta");
				System.out.println("===Pulse cualquier otra tecla para salir ==");

				int opcion = Integer.valueOf(in.nextLine());
				// gestionMateriales();

				switch (opcion) {
				case 1:
					gestionUsuarios();
					break;
				case 2:
					gestionMateriales();
					break;
				case 3:
					gestionSuscripciones();
					break;
				case 4:
					gestionPrestamos();
				case 5:
					//b.getTarjeta();
					break;
				}
			} while (!salir && menuBibliotecario);
		}
		
		if(menuSocio) {
			do {
				System.out.println("1.Ver prestamos");
				System.out.println("2.Comprobar avisos.");
				System.out.println("3.Listado de materiales");
				System.out.println("4.Tarjeta");

				int opcion = Integer.valueOf(in.nextLine());
				switch(opcion) {
				case 1:
					if(!(s.getPrestamos().isEmpty())){
						for (Prestamo p : s.getPrestamos()) {
							System.out.println(p.toString());
						}
					}
					break;
				case 2:
					for(String aviso : s.getAvisosReserva()) {
						System.out.println("Aviso: " + aviso);
					}
					break;
				case 3:
					System.out.println("==========LIBROS==============");
					this.sgib.listarLibros();
					System.out.println();
					System.out.println("==========REVISTAS==============");
					this.sgib.listarRevistas();
					System.out.println();
					System.out.println("==========PERIODICOS==============");
					this.sgib.listarPeriodicos();
					System.out.println();
					System.out.println("==========AUDIO==============");
					this.sgib.listarAudio();
					System.out.println();
					System.out.println("==========VIDEO==============");
					this.sgib.listarVideo();
					System.out.println();
					break;
				}

			}while(!salir && menuSocio);
		}
	}
	
	
	/*
	 * Pide un usuario y contraseña y los manda al controlador para efectuar el login
	 */
	public Usuario login() {
		System.out.println("S/B: ");
		String tipo = in.nextLine().toLowerCase().trim();
		System.out.println("Introduzca usuario: \n");
		String usuario = in.nextLine();
		System.out.println("Introduzca contraseña: \n");
		String pass = in.nextLine();
		return this.sgib.loguear(tipo, usuario,pass);
		
	}
	
	//MENU GESTION DE USUARIOS, SOLO PARA BIBLIOTECARIOS
	public void gestionUsuarios() {
		System.out.println("1.Alta socio");
		System.out.println("2.Alta bibliotecario");
		System.out.println("3.Baja usuario");
		System.out.println("4.Lista usuarios");
		int opcion = Integer.valueOf(in.nextLine());
		//gestionMateriales();
		String usuario;
		String pass;
		
		switch(opcion) {
		case 1:
			System.out.println("Introduzca usuario: \n");
			usuario = in.nextLine();
			System.out.println("Introduzca contraseña: \n");
			pass = in.nextLine();
			this.sgib.altaUsuario(new Socio(usuario,pass));
			break;
		case 2:
			System.out.println("Introduzca usuario: \n");
			usuario = in.nextLine();
			System.out.println("Introduzca contraseña: \n");
			pass = in.nextLine();
			this.sgib.altaUsuario(new Bibliotecario(usuario,pass));
			break;
		case 3:
			this.sgib.listarUsuarios();
			System.out.println();
			System.out.println("Introduzca el ID del usuario: ");
			String id = in.nextLine();
			this.sgib.bajaUsuario(id);
			break;
		case 4:
			this.sgib.listarUsuarios();
			break;
		default:
			System.out.println("Saliendo gestor usuarios..");
			break;
		}		
		
	}
	
	//MENUS MATERIALES SOLO BILBIOTECARIOS
	public void gestionMateriales() {

	   System.out.println("1.ALTA MATERIAL");
	   System.out.println("2.BAJA MATERIAL");
	   System.out.println("3.LISTA MATERIALES");
	   System.out.println("4.BUSQUEDA POR FILTROS");
	   System.out.println("5.BUSQUEDA SIMPLE");
	   System.out.println("===Pulse cualquier otra tecla para salir ==");
	   int opcion = Integer.valueOf(in.nextLine());
	   
	   switch(opcion) {
	   case 1:
		   altaMaterial();
		   break;
	   case 2:
		   borrarMaterial();
		   break;
	   case 3:
		   listarMaterial();
		   break;
	   case 4:
		   buscarMaterial();
		   break;
	   case 5:
		   buscarID();
		   break;
	   default:
		   this.salir = true;
		   break;	   
	   }
	}

	public void altaMaterial() {
		
		   System.out.println("1.LIBRO");
		   System.out.println("2.REVISTA");
		   System.out.println("3.PERIODICO");
		   System.out.println("4.AUDIO");
		   System.out.println("5.VIDEO");
		   System.out.println("Pulse cualquier tecla para volver");
		   int opcion = Integer.valueOf(in.nextLine());
		   String datos[];
		   
		   switch(opcion) {
		   
		   case 1:		   
			   System.out.println("Introduce en el siguiente orden y separado por comas:");
			   System.out.println("Nombre,ISBN,editorial,autor \n");
			   //Lee datos
			   datos = in.nextLine().split(",");
			   //Llamamos al controlador y le pasamos el objeto creado
			   Libro l = new Libro(datos[0],datos[1],datos[2],datos[3]);
			   this.sgib.altaMaterial(l);
			   break;
		   case 2:
			   System.out.println("Introduce en el siguiente orden y separado por comas:");
			   System.out.println("Nombre de la revista, temática. \n");
			   //Recoge datos
			   datos = in.nextLine().split(",");
			   //Creamos el objeto y se lo pasamos al constructor
			   Revista r = new Revista(datos[0],datos[1]);
			   this.sgib.altaMaterial(r);
			   break;
		   case 3:
			   System.out.println("Introduce en el siguiente orden y separado por comas:");
			   System.out.println("Nombre del periodico, fecha impresion. \n");
			   //Recoge datos
			   datos = in.nextLine().split(",");
			   //Creamos el objeto y se lo pasamos al constructor
			   Periodico p = new Periodico(datos[0],datos[1]);
			   this.sgib.altaMaterial(p);
			   break;
		   case 4:
			   System.out.println("Introduce en el siguiente orden y separado por comas:");
			   System.out.println("Nombre del disco, autor. \n");
			   //Recoge datos
			   datos = in.nextLine().split(",");
			   //Creamos el objeto y se lo pasamos al constructor
			   Audio a = new Audio(datos[0],datos[1]);
			   this.sgib.altaMaterial(a);
			   break;
		   case 5:
			   System.out.println("Introduce en el siguiente orden y separado por comas:");
			   System.out.println("Nombre del film, autor. \n");
			   //Recoge datos
			   datos = in.nextLine().split(",");
			   //Creamos el objeto y se lo pasamos al constructor
			   Video v = new Video(datos[0],datos[1]);
			   this.sgib.altaMaterial(v);
			   break;
		   default:
			   break;
		   }	   
	}
	public void borrarMaterial() {
		System.out.println("==========LIBROS==============");
		this.sgib.listarLibros();
		System.out.println();
		System.out.println("==========REVISTAS==============");
		this.sgib.listarRevistas();
		System.out.println();
		System.out.println("==========PERIODICOS==============");
		this.sgib.listarPeriodicos();
		System.out.println();
		System.out.println("==========AUDIO==============");
		this.sgib.listarAudio();
		System.out.println();
		System.out.println("==========VIDEO==============");
		this.sgib.listarVideo();
		System.out.println();
		System.out.println("Introduce el ID completo: ");
		String id = in.nextLine();
		this.sgib.borrarMaterial(id);
	}
	public void listarMaterial() {
		   System.out.println("1.LIBRO");
		   System.out.println("2.REVISTA");
		   System.out.println("3.PERIODICO");
		   System.out.println("4.AUDIO");
		   System.out.println("5.VIDEO");
		   System.out.println("6.TODOS");
		   
		   int opcion = Integer.valueOf(in.nextLine());
		   
		   switch(opcion) {
		   
		   case 1:		   
			   this.sgib.listarLibros();
			   break;
		   case 2:
			   this.sgib.listarRevistas();
			   break;
		   case 3:
			   this.sgib.listarPeriodicos();
			   break;
		   case 4:
			   this.sgib.listarAudio();
			   break;
		   case 5:
			   this.sgib.listarVideo();
			   break;
		   case 6:
			   mostrarTodoMaterial();
			   break;
		   default:
			   break;
		   }	   
	}
	public void buscarID() {
		System.out.println("Introduce el ID completo: ");
		String id = in.nextLine();
		System.out.println(this.sgib.busquedaSimple(id).toString());
	}
	public void buscarMaterial() {
		
		   System.out.println("1.LIBRO");
		   System.out.println("2.REVISTA");
		   System.out.println("3.PERIODICO");
		   System.out.println("4.AUDIO");
		   System.out.println("5.VIDEO");
		   
		   int opcion = Integer.valueOf(in.nextLine());
		   String datos[];
		   
		   switch(opcion) {
		   case 1:
			datos = new String[4];
			System.out.println("Pulse enter en los campos que no quiera filtrar.");

			System.out.println("Titulo: ");
			datos[0] = in.nextLine();
			System.out.println("ISBN: ");
			datos[1] = in.nextLine();
			System.out.println("Editorial: ");
			datos[2] = (in.nextLine());
			System.out.println("Autor: ");
			datos[3] = (in.nextLine());
			//cambia los espacios en blanco por null para la busqueda
			datos = LecturaConsola.comprobarEspacios(datos);
			LecturaConsola.imprimirLista(this.sgib.busquedaFlexible(new Libro(datos[0],datos[1],datos[2],datos[3])));
			break;
			
		   case 2:
			   datos = new String[2];
			   System.out.println("Pulse enter en los campos que no quiera filtrar.");
			   System.out.println("Nombre: ");
			   datos[0] = in.nextLine();
			   System.out.println("Temática: ");
			   datos[1] = in.nextLine();
			   
			   datos = LecturaConsola.comprobarEspacios(datos);
			   LecturaConsola.imprimirLista(this.sgib.busquedaFlexible(new Revista(datos[0],datos[1])));
			   break;
		   case 3:
			   datos = new String[2];
			   System.out.println("Pulse enter en los campos que no quiera filtrar.");
			   System.out.println("Nombre: ");
			   datos[0] = in.nextLine();
			   System.out.println("Fecha: (dd/mm/aaaa)");
			   datos[1] = in.nextLine();
			   
			   datos = LecturaConsola.comprobarEspacios(datos);
			   LecturaConsola.imprimirLista(this.sgib.busquedaFlexible(new Periodico(datos[0],datos[1])));
			   break;
		   case 4:
			   datos = new String[2];
			   System.out.println("Pulse enter en los campos que no quiera filtrar.");
			   System.out.println("Nombre: ");
			   datos[0] = in.nextLine();
			   System.out.println("Autor:");
			   datos[1] = in.nextLine();
			   
			   datos = LecturaConsola.comprobarEspacios(datos);
			   LecturaConsola.imprimirLista(this.sgib.busquedaFlexible(new Audio(datos[0],datos[1])));
			   break;
		   case 5:
			   datos = new String[2];
			   System.out.println("Pulse enter en los campos que no quiera filtrar.");
			   System.out.println("Nombre: ");
			   datos[0] = in.nextLine();
			   System.out.println("Director:");
			   datos[1] = in.nextLine();
			   
			   datos = LecturaConsola.comprobarEspacios(datos);
			   LecturaConsola.imprimirLista(this.sgib.busquedaFlexible(new Video(datos[0],datos[1])));
		   }
	}//public Libro(String nombre, String isbn, String editorial, String autor)
	
	public void mostrarTodoMaterial() {
		System.out.println("==========LIBROS==============");
		this.sgib.listarLibros();
		System.out.println();
		System.out.println("==========REVISTAS==============");
		this.sgib.listarRevistas();
		System.out.println();
		System.out.println("==========PERIODICOS==============");
		this.sgib.listarPeriodicos();
		System.out.println();
		System.out.println("==========AUDIO==============");
		this.sgib.listarAudio();
		System.out.println();
		System.out.println("==========VIDEO==============");
		this.sgib.listarVideo();
		System.out.println();
	}
	//FIN AMTERIALES
	
	//MENU SUSCRIPCIONES
	public void gestionSuscripciones() {
		   System.out.println("1.Ver suscripciones");
		   System.out.println("2.Nueva suscripcion");
		   System.out.println("3.Cancelar suscripcion");

		   int opcion = Integer.valueOf(in.nextLine());
		   
		   switch(opcion) {
		   
		   case 1:
			   this.sgib.listarSuscribibles();
			   break;
		   case 2:
			   altaSuscripcion();
			   break;
		   case 3:
			   this.sgib.listarSuscribibles();
			   System.out.println();
			   System.out.println("Introduce el ID: ");
			   String id = in.nextLine().trim();
			   this.sgib.bajaSuscripcion(id);
			   break;
		   case 4:
			   
			   break;
		   default:
			   break;
		   }	
	}
	//String nombre, String tematica, String periodo, double precio
	public void altaSuscripcion() {
		String datos[];
		System.out.println("Suscripcion tipo revista, pulse 1 /Suscripcion tipo periodico, pulse 2");
		int opcion = Integer.valueOf(in.nextLine());
		switch(opcion) {
		case 1:
			System.out.println("Introduce separado por comas:");
			System.out.println("Nombre,tematica,periodo en el que llegan los numeros,precio.");
			datos = in.nextLine().split(",");
			Revista r = new Revista(datos[0],datos[1],datos[2],Double.valueOf(datos[3]));
			this.sgib.altaSuscripcion(r);
			break;
		case 2:
			System.out.println("Introduce separado por comas:");
			System.out.println("Nombre, Fecha impresion, periodo en el que llegna los numeros, precio.");
			datos = in.nextLine().split(",");
			Periodico p = new Periodico(datos[0],datos[1],datos[2],Double.valueOf(datos[3]));
			this.sgib.altaSuscripcion(p);
			break;
		default:
			System.out.println("saliendo del menu..");
			break;
		}
	}
	//FIN SUSCRIPCIONES
	//PRESTAMOS
	public void gestionPrestamos() {
		   System.out.println("1.Nuevo prestamo");
		   System.out.println("2.Finalizar prestamo");
		   System.out.println("3.Visualizar prestamos");
		   
		   int opcion = Integer.valueOf(in.nextLine());
		   
		   switch(opcion) {
		   case 1:
			   nuevoPrestamo();
			   break;
		   case 2:
			   finalizarPrestamo();
			   break;
		   case 3:
			   visualizacionPrestamos();
			   break;
		   }
	}
	
	public void nuevoPrestamo() {
		
		   System.out.println("1.LIBRO");
		   System.out.println("2.REVISTA");
		   System.out.println("3.PERIODICO");
		   System.out.println("4.AUDIO");
		   System.out.println("5.VIDEO");
		   
		   int opcion = Integer.valueOf(in.nextLine());
		   String datos[];
		   
		   switch(opcion) {
		   case 1:
			   this.sgib.listarLibros();
			   System.out.println();
			   this.sgib.listarUsuarios();
			   System.out.println();
			   System.out.println("Nombre del socio y introduce id del material(debe estar disponible). (separe con comas)");
			   datos = in.nextLine().trim().split(",");
			   this.sgib.nuevoPrestamo(datos[0],datos[1]);
			   break;
		   case 2:
			   this.sgib.listarRevistas();
			   System.out.println();
			   this.sgib.listarUsuarios();
			   System.out.println();
			   System.out.println("Nombre del socio y introduce id del material(debe estar disponible). (separe con comas)");
			   datos = in.nextLine().trim().split(",");
			   this.sgib.nuevoPrestamo(datos[0],datos[1]);
			   break;
		   case 3:
			   this.sgib.listarPeriodicos();
			   System.out.println();
			   this.sgib.listarUsuarios();
			   System.out.println();
			   System.out.println("Nombre del socio y introduce id del material(debe estar disponible). (separe con comas)");
			   datos = in.nextLine().trim().split(",");
			   this.sgib.nuevoPrestamo(datos[0],datos[1]);
			   break;
		   case 4:
			   this.sgib.listarAudio();
			   System.out.println();
			   this.sgib.listarUsuarios();
			   System.out.println();
			   System.out.println("Nombre del socio y introduce id del material(debe estar disponible). (separe con comas)");
			   datos = in.nextLine().trim().split(",");
			   this.sgib.nuevoPrestamo(datos[0],datos[1]);
			   break;
		   case 5:
			   this.sgib.listarVideo();
			   System.out.println();
			   this.sgib.listarUsuarios();
			   System.out.println();
			   System.out.println("Nombre del socio y introduce id del material(debe estar disponible). (separe con comas)");
			   datos = in.nextLine().trim().split(",");
			   this.sgib.nuevoPrestamo(datos[0],datos[1]);
			   break;
		   }
	}
	
	public void visualizacionPrestamos() {
		   System.out.println("1. Filtro por tipo de material.");
		   System.out.println("2. Ver prestamos de socio.");
		   System.out.println("3. Ver todos los prestamos actuales.");
		   //this.sgib.listarPrestamos();
		   
		   int opcion = Integer.valueOf(in.nextLine());
		   
		   switch(opcion) {
		   case 1:
			   verPrestamoTipo();
			   break;
		   case 2:
			   verPrestamoUsuario();
			   break;
		   case 3:
			   this.sgib.listarPrestamos();
			   break;
		   }
	}
	public void verPrestamoUsuario() {
		   System.out.println();
		   this.sgib.listarUsuarios();
		   System.out.println();
		   System.out.println("Nombre del socio: ");
		   String datos = in.nextLine().trim();
		   
		   this.sgib.mostrarPrestamosSocio(datos);
		   
	}
	public void verPrestamoTipo() {
		   System.out.println("1.LIBRO");
		   System.out.println("2.REVISTA");
		   System.out.println("3.PERIODICO");
		   System.out.println("4.AUDIO");
		   System.out.println("5.VIDEO");
		   
		   int opcion = Integer.valueOf(in.nextLine());
		   
		   switch(opcion){
		   case 1:
			   this.sgib.listarPrestamosLibro();
			   break;
		   case 2:
			   this.sgib.listarPrestamosRevista();
			   break;
		   case 3:
			   this.sgib.listarPrestamosPeriodico();
			   break;
		   case 4:
			   this.sgib.listarPrestamosAudio();
			   break;
		   case 5:
			   this.sgib.listarPrestamoVideo();
			   break;
		   }
	}
	public void finalizarPrestamo() {
		System.out.println();
		this.sgib.listarUsuarios();
		System.out.println();
		System.out.println("Nombre del socio: ");
		String socioNombre = in.nextLine().trim();
		System.out.println();
		System.out.println("===========PRESTAMOS ACTIVOS==============");
		this.sgib.mostrarPrestamosSocio(socioNombre);
		System.out.println();
		
		System.out.println("Introduce el id del prestamo: ");
		String idPrestamo = in.nextLine().trim();
		this.sgib.devolverPrestamo(idPrestamo, socioNombre);
	}
	//FIN
}
