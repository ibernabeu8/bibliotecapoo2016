package controlador;

import java.util.ArrayList;
import java.util.Iterator;

import gestorMateriales.Audio;
import gestorMateriales.GestorMateriales;
import gestorMateriales.Libro;
import gestorMateriales.Material;
import gestorMateriales.Periodico;
import gestorMateriales.Revista;
import gestorMateriales.Suscripcion;
import gestorMateriales.Video;
import gestorPrestamos.GestorPrestamos;
import gestorPrestamos.Prestamo;
import gestorUsuarios.Bibliotecario;
import gestorUsuarios.GestorUsuarios;
import gestorUsuarios.Socio;
import gestorUsuarios.Usuario;

public class SGIB {
	//Atributos
	private GestorMateriales gestorMateriales;
	private GestorUsuarios gestorUsuarios;
	private GestorPrestamos gestorPrestamos;
	//constructor
	public SGIB() {
		this.gestorMateriales = new GestorMateriales();
		this.gestorUsuarios = new GestorUsuarios();
		this.gestorPrestamos = new GestorPrestamos();
	}
	//logueos
	public Usuario loguear(String tipo, String nombre, String pass) {
		
		switch(tipo)
		{
		case "s":
			Socio socio = new Socio(nombre,pass);
			if(this.gestorUsuarios.login(socio)) return this.gestorUsuarios.buscarSocio(nombre);
		case "b":
			Bibliotecario bibliotecario = new Bibliotecario(nombre,pass);
			if(this.gestorUsuarios.login(bibliotecario)) return bibliotecario;
		default:
			System.out.println("tipo de usuario no existe.");
			return null;
		}
	}
	
	//operacioens en usuarios
	public void altaUsuario(Usuario u) {
		Usuario nuevaAlta = null;
		if(u instanceof Bibliotecario) {
			nuevaAlta = (Bibliotecario)u;
		}
		if(u instanceof Socio) {
			nuevaAlta = (Socio)u;
		}
		this.gestorUsuarios.altaUsuario(nuevaAlta);
	}
	public void bajaUsuario(String id) {
		this.gestorUsuarios.bajaUsuario(id);
	}
	//operaciones sobre materiales
	public void altaMaterial(Material m) {
		this.gestorMateriales.altaMaterial(m);
	}
	
	public void borrarMaterial(String id) {
		if(id != null)
			this.gestorMateriales.borrar(id);
	}
	
	public void altaSuscripcion(Suscripcion s) {
		if(s instanceof Revista || s instanceof Periodico) {
			s.setActiva(true);
			this.gestorMateriales.altaSuscripcion(s);
		}
	}
	public void bajaSuscripcion(String id) {
		this.gestorMateriales.borrarSuscripcion(id);
	}
	//busuqeda flexible y simple
	public Material busquedaSimple(String id) {
		Material m = this.gestorMateriales.busquedaSimple(id);
		if(m != null) return m;
		else
			return null;
	}
	public ArrayList<? extends Material> busquedaFlexible(Material m) {
		if(m instanceof Libro) {
			Libro l = (Libro)m;
			return this.gestorMateriales.buscarLibro(l);			
		}
		else if(m instanceof Revista) {
			Revista r = (Revista)m;
			return this.gestorMateriales.buscarRevista(r);
		}
		else if(m instanceof Periodico) {
			Periodico p = (Periodico)m;
			return this.gestorMateriales.buscarPeriodico(p);
		}
		else if(m instanceof Audio) {
			Audio a = (Audio)m;
			return this.gestorMateriales.buscarAudio(a);
		}
		else if(m instanceof Video) {
			Video v = (Video)m;
			return this.gestorMateriales.buscarVideo(v);
		}else{
			System.out.println("No se encuentra el tipo de material especificado.");
			return null;
		}
	}
	//diferentes listados
	public void listarLibros() {
		this.gestorMateriales.listar(GestorMateriales.TIPO_LIBRO);
	}
	public void listarRevistas() {
		this.gestorMateriales.listar(GestorMateriales.TIPO_REVISTA);
	}
	public void listarPeriodicos() {
		this.gestorMateriales.listar(GestorMateriales.TIPO_PERIODICO);
	}
	public void listarAudio() {
		this.gestorMateriales.listar(GestorMateriales.TIPO_AUDIO);
	}
	public void listarVideo() {
		this.gestorMateriales.listar(GestorMateriales.TIPO_VIDEO);
	}
	public void listarSuscribibles() {
		this.gestorMateriales.listar(GestorMateriales.TIPO_SUSCRIPCION);
	}
	public void listarUsuarios() {
		Iterator<Usuario> it = this.gestorUsuarios.getListado().iterator();
		while(it.hasNext()) {
			System.out.println(it.next().toString());
		}	
	}
	public void listarPrestamos() {
		System.out.println(this.gestorPrestamos.getLista().size());
		Iterator<Prestamo> it = this.gestorPrestamos.getLista().iterator();
		while(it.hasNext()){
			System.out.println(it.next().toString());
		}
	}

	
	//PRESTAMOS
	public boolean nuevoPrestamo(String nombreSocio, String materialID){
		Socio socio = this.gestorUsuarios.buscarSocio(nombreSocio);
		Material m = this.gestorMateriales.busquedaSimple(materialID);

		if(socio != null && socio.getPrestamos().size() < 6  && m.isDisponible()) {
			System.out.println("Guardando..");
			//cambiamos la disponibilidad
			this.gestorMateriales.borrar(m.getId());
			m.setDisponible(false);
			this.gestorMateriales.altaMaterial(m);
			//creamos el prestamo
			Prestamo p = this.gestorPrestamos.nuevoPrestamo(socio.getID(), m);
			//lo añadimos al socio
			socio.nuevoItem(p);
			this.gestorUsuarios.actualizar(socio);
			return true;
		}
		System.out.println("El material no se encuentra disponible o el usuario no es válido.");
		return false;
	}
	public void devolverPrestamo(String idPrestamo, String nombreSocio) {
		Socio s = this.gestorUsuarios.buscarSocio(nombreSocio);
		//System.out.println(s.getNombre());
		if(!(s.equals(null))) {
			ArrayList<Prestamo> prestamosActualizados = (ArrayList<Prestamo>) s.getPrestamos().clone();
			Iterator<Prestamo> it = s.getPrestamos().iterator();
			while(it.hasNext())  {
				Prestamo p = it.next();
				if(p.getId().equals(idPrestamo)) {
					prestamosActualizados.remove(p);
				}
			}
			s.setPrestamos(prestamosActualizados);
			//acutalizamos el usuario
			
			this.gestorUsuarios.actualizar(s);
			//borramos de la lista de prestamos
			Material m = this.gestorPrestamos.devolverPrestamo(idPrestamo);
			this.gestorMateriales.cambiarDisponibilidad(m, true);
		}
	}
	public void listarPrestamosLibro() {
		System.out.println("========PRESTAMOS DE LIBROS ==========");
		for(Prestamo p : this.gestorPrestamos.getLista()) {
			if(p.getMat() instanceof Libro) {
				Libro l = (Libro) p.getMat();
				System.out.println(p.toString());
				System.out.println(l.toString());
				System.out.println();
			}
		}
		System.out.println("=======================================");
	}
	public void listarPrestamosRevista() {
		System.out.println("========PRESTAMOS DE REVISTA ==========");
		for(Prestamo p : this.gestorPrestamos.getLista()) {
			if(p.getMat() instanceof Revista) {
				Revista r = (Revista) p.getMat();
				System.out.println(p.toString());
				System.out.println(r.toString());
				System.out.println();
			}
		}
		System.out.println("=======================================");
	}
	public void listarPrestamosPeriodico() {
		System.out.println("========PRESTAMOS DE PERIODICO ==========");
		for(Prestamo p : this.gestorPrestamos.getLista()) {
			if(p.getMat() instanceof Libro) {
				Periodico periodico = (Periodico) p.getMat();
				System.out.println(p.toString());
				System.out.println(periodico.toString());
				System.out.println();
			}
		}
		System.out.println("=======================================");
	}
	public void listarPrestamoVideo() {
		System.out.println("========PRESTAMOS DE VIDEO ==========");
		for(Prestamo p : this.gestorPrestamos.getLista()) {
			if(p.getMat() instanceof Libro) {
				Video v = (Video) p.getMat();
				System.out.println(p.toString());
				System.out.println(v.toString());
				System.out.println();
			}
		}
		System.out.println("=======================================");
	}
	public void listarPrestamosAudio() {
		System.out.println("========PRESTAMOS DE LIBROS ==========");
		for(Prestamo p : this.gestorPrestamos.getLista()) {
			if(p.getMat() instanceof Libro) {
				Audio a = (Audio) p.getMat();
				System.out.println(p.toString());
				System.out.println(a.toString());
				System.out.println();
			}
		}
		System.out.println("=======================================");
	}
	public void mostrarPrestamosSocio(String nombreSocio) {
		//System.out.println(this.gestorUsuarios.buscarSocio(nombreSocio).toString());
		Iterator<Prestamo> it = this.gestorUsuarios.buscarSocio(nombreSocio).getPrestamos().iterator();
		//System.out.println(it.next().toString());
		while(it.hasNext()) {
			System.out.println(it.next().toString());
		}
	}
}
