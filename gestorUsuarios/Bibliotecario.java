package gestorUsuarios;

public class Bibliotecario extends Usuario{

	public Bibliotecario(String nombre, String pass) {
		super(nombre,pass);
	}

	@Override
	public String tarjeta() {
		String tarjeta = "=============================================================="
				+ "					BIBLIOTECARIO										"
				+ "======================================================================"
				+ "CODIGO: " + id + "			NOMBRE: " + nombre
				+ "========================================================================";
		return tarjeta;
	}
}
