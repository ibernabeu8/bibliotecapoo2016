package gestorUsuarios;

import java.util.Iterator;
/*
 * UTILIZAR ESTA CLASE PARA CREAR EL PRIMER USUARIO, ADMIN
 */

public class PruebasUsuarios {

	public static void main(String[] args) {
		GestorUsuarios gu = new GestorUsuarios();

		
		Bibliotecario b = new Bibliotecario("admin","admin");
		System.out.println(b.tarjeta());
		gu.altaUsuario(new Bibliotecario("admin","admin"));
		
		
		Iterator<Usuario> it = gu.getListado().iterator();
		
		while(it.hasNext()) {
			Usuario u = it.next();
			System.out.println(u.getNombre() + "  " + u.getPass() + "\n");
		}

	}

}
