package gestorUsuarios;

import java.util.ArrayList;

import gestorPrestamos.Prestamo;

public class Socio extends Usuario{
	
	
	public Socio(String nombre, String pass) {
		super(nombre,pass);
	}
	//propiedades
	private ArrayList<Prestamo> historialPrestamos = new ArrayList<Prestamo>();
	private ArrayList<Prestamo> prestamos = new ArrayList<Prestamo>();//inicializamos el array con un numero maximo de 6
    private ArrayList<String> avisosReserva;
    
    
    public void nuevoItem(Prestamo p) {
    	if(prestamos.size() < 6) {
    		this.prestamos.add(p);
    	}
    }
    public void eliminarItem(Prestamo p) {
    	this.prestamos.remove(p);
    	this.historialPrestamos.add(p);
    }
    //gets
	public ArrayList<Prestamo> getPrestamos() {
		return prestamos;
	}
	public ArrayList<String> getAvisosReserva() {
		return avisosReserva;
	}
	public void setPrestamos(ArrayList<Prestamo> prestamos) {
		this.prestamos = prestamos;
	}
	
	public void setAvisosReserva(String aviso) {
		this.avisosReserva.add(aviso);
	}
	public void setAvisosReserva(ArrayList<String> avisosReserva) {
		this.avisosReserva = avisosReserva;
	}

	public ArrayList<Prestamo> getHistorialPrestamos() {
		return historialPrestamos;
	}
	public void setHistorialPrestamos(ArrayList<Prestamo> historialPrestamos) {
		this.historialPrestamos = historialPrestamos;
	}
	@Override
	public String tarjeta() {
		// TODO Auto-generated method stub
		return null;
	}
	/*
	 * Devuelve true si coincide el ID o el NOMBRE, ya que al guaradr los usuarios lo ahcemos en un hashset , y que 
	 * el HASHCODE  lo generamos con el metodo hashcode de la clase padre utilziando el NOMBRE, no podra haber dos usuarios
	 * con el mismo nombre.
	 * @see gestorUsuarios.Usuario#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		
		if(!(obj instanceof Socio))
			return false;
		
		Socio otro = (Socio)obj;
		
		if(otro.id.equals(id)) return true;
		
		else if(otro.nombre.equals(obj)) return true;
		
		else 
			return false;
	}
    
    
}
