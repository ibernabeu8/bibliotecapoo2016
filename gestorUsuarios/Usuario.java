package gestorUsuarios;
import java.io.Serializable;
import java.util.UUID;

public abstract class Usuario implements Serializable {

	//propiedades
	protected String id;
	protected String nombre;
	protected String pass;

	//
	public Usuario(String id) {
		this.id = id;
	}
	
	public Usuario(String nombre, String pass) {
		this.id = UUID.randomUUID().toString();
		this.nombre = nombre;
		this.pass = pass;
	}
	
	//metodos
	public String getID() {
		return this.id;
	}
	public String getNombre() {
		return this.nombre;
	}
	public String getPass() {
		return this.pass;
	}
	
	//
	public String toString() {
		return "Cod: " + this.id + " Nombre: " + this.nombre;
	}
	//
	public abstract String tarjeta();
	//haciendo el hashcode con el nombre evitamos que un usuario pueda tener el mismo nombre
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Usuario))
			return false;
		Usuario otro = (Usuario) obj;
		
		if(nombre.equals(otro.nombre))
			return true;
		return false;
	}
	
	
}
