package gestorUsuarios;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import gestorPrestamos.Prestamo;

public class GestorUsuarios {
	
	//PROPIEDADES
	private final String URL = "usr/users.dat";
	//lista
	private HashSet<Usuario> listado;
	//
	//Streams de escritura
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	
	//constructor
	
	public GestorUsuarios() {
		init();
	}
	
	private void init() {
		utilidades.GeneradorURL.crearRuta(URL);
		cargar();
	}
	public HashSet<Usuario> getListado() {
		return this.listado;
	}
	//comprueba el tipo de usuario que es y lo registra en el sistema
	public void altaUsuario(Usuario u) {
		/*if(!(this.listado.contains(u))) {
			this.listado.add(u);
			guardar();
		}*/
		Iterator<? extends Usuario> it = this.listado.iterator();
		boolean noGuardar = false;
		while(it.hasNext()) {
			Usuario guardado = it.next();
			if(guardado.getNombre().equals(u.nombre))
				noGuardar = true;
		}
		if(!noGuardar){
			this.listado.add(u);
			guardar();
		}
	}
	public void bajaUsuario(String id) {
		Iterator<? extends Usuario> it = this.listado.iterator();
		while(it.hasNext()) {
			Usuario u = it.next();
			if(u.id.equals(id)) {
				this.listado.remove(u);
				guardar();
			}
		}
	}
	//comprueba si existe el usuario en el listado guardado y devuelve un booleano si es correcto
	public boolean login(Usuario u) {
		Iterator<? extends Usuario> it = this.listado.iterator();
		
		while(it.hasNext()) {
			Usuario uGuardado = it.next();
			if(u.getNombre().equals(uGuardado.getNombre()) && u.getPass().equals(uGuardado.getPass()))
				return true;
		}
		return false;
	}
	public Socio buscarSocio(String nombre) {
		Iterator<? extends Usuario> it = this.listado.iterator();
		while(it.hasNext()) {
			//System.out.println("Funciona?");
			Usuario siguiente = it.next();
			//System.out.println(siguiente.getNombre());
			if( siguiente instanceof Socio) {
				Socio s = (Socio) siguiente;
				if(s.getNombre().equals(nombre)) return s;
			}
		}
		System.out.println("esta devolviendo null?");
		return null;
	}
	public void actualizar(Socio s) {
		this.listado.remove(s);
		this.listado.add(s);
		guardar();
	}
	/*
	public void nuevoPrestamoUsuario(Socio s) {
		this.listado.remove(s);
		this.listado.add(s);
		guardar();
	}*/
	
	public void devolverPrestamoUsuario(Socio s, String idPrestamo) {
		this.listado.remove(s);
		this.listado.add(s);
		guardar();
	}
	/*mandar aviso
	public void avisos(String aviso, Socio s) {
		Socio buscado = buscarSocio(s.id, s.nombre);
		if(buscado != null) {
			this.listado.remove(buscado);
			buscado.setAvisosReserva(aviso);
			this.listado.add(buscado);
			guardar();
		}
	}*/
	
	//metodos para guardar y cargar el listado de objetos usuario
	private void guardar() {
		try{
			this.oos = new ObjectOutputStream(new FileOutputStream(this.URL));
			this.oos.writeObject(this.listado);
			this.oos.close();
		}catch (EOFException e) {
			System.out.println("Final de archivo.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void cargar() {
		try {
			this.listado = new HashSet<Usuario>();
			this.ois = new ObjectInputStream(new FileInputStream(this.URL));
			this.listado = (HashSet<Usuario>) this.ois.readObject();
			this.ois.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (EOFException e) {
			System.out.println("Final de archivo.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
