package utilidades;

public class UltimaEscritaArray {
	
	public static int ultimaPosicion(Object[] array) {
		
		
		for(int i = 0; i < array.length; i++) {
			
			if(array[i] != null) return i;
		}
		return 0;
	}
}
