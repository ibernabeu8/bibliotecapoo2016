package utilidades;

import java.util.List;

import gestorMateriales.Material;

public class LecturaConsola {
	
	public static String[] comprobarEspacios(String[] array) {
		
		for(int i = 0; i < array.length; i++) {
			if(array[i].equals(""))
				array[i] = null;
		}
		return array;
	}
	
	public static void imprimirLista(List<? extends Material> lista) {
		for(Material m: lista) {
			System.out.println(m.toString());
		}
	}
}
