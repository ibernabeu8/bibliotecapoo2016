package utilidades;

import java.io.File;
import java.io.IOException;

public class GeneradorURL {

	public static void crearRuta(String url) {
		String directorio = url.split("/")[0];
		
		File f = new File(directorio);	
		if(!f.exists()) f.mkdirs();
		
		f = new File(url);
		
		if(!f.exists())
			try {
				f.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
}
