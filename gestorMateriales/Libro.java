package gestorMateriales;

public class Libro extends Material {
	
	//propiedades
	private String isbn, autor, editorial;
	
	//Constructor
	public Libro() {}
	
	public Libro(String id) {
		this.id = id;
	}
	public Libro(String nombre, String isbn, String editorial, String autor) {
		super(nombre);
		this.isbn = isbn;
		this.autor = autor;
		this.editorial = editorial;
	}
	//gets & sets
	public String getIsbn() {
		return isbn;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	@Override
	public String toString() {
		return "Libro, COD: " + getId() + " ISBN: " + isbn + " Titulo: " + getNombre() + " Autor: " + getAutor() + " Editorial: " + editorial
				+ " Disponibilidad: " + isDisponible();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((autor == null) ? 0 : autor.hashCode());
		result = prime * result + ((editorial == null) ? 0 : editorial.hashCode());
		result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
		return result;
	}
	/**
	 * He sobrescrito el metodo para que simplifique las busquedas simples y flexibles, lo que hace es comparar las
	 * propiedades que he considerado para decir si un libro es igual a otro, si son nulas las ignora y si no las compara
	 * y almacena true o false en un array booleano, al final lo que hago es comprobar si existe algun falso en ese array
	 * eso indicara que una de las propiedades con las que se ha comparado no coincide y por lo tanto no es el buscado,
	 * en cambio si algunas propiedades eran null y las que no lo eran coincidian devolvera true.
	 */
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Libro)) {
			return false;
		}
		Libro otro = (Libro)obj;
		//utilizamos el objeto BOOLEAN ya que el tipo primitivo se inicia en false
		boolean[] propiedades = {true,true,true,true};
		//si el id es igual deolvevemos directamente true
		if(otro.id.equals(id)) return true;
		if(otro.nombre != null) {
			propiedades[0] = nombre.equals(otro.nombre);
		}
		if(otro.isbn != null) {
			propiedades[1] = isbn.equals(otro.isbn);
		}	
		if(otro.editorial != null) {
			propiedades[2] = editorial.equals(otro.editorial);
		}
		if(otro.autor != null) {
			propiedades[3] = autor.equals(otro.autor);
		}
		
		for(boolean propiedad : propiedades) {
			//System.out.println("entra4?");
			if(propiedad == false) {
				//System.out.println(propiedad);
				return false;
			}			
		}
		return true;
	}
}
