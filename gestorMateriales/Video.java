package gestorMateriales;

public class Video extends Material{
	
	//propiedades
	private String director;
	//constructores
	public Video(){}
	public Video(String nombre, String director) {
		super(nombre);
		this.director = director;
	}
	
	//gets && sets
	public String getDirector() {
		return this.director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	@Override
	public String toString() {
		return "Video, COD: " + id + " Nombre: " + nombre + " Director: " + director + "  Disponible: " + disponible;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((director == null) ? 0 : director.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof Video))
			return false;
		
		Video otro = (Video) obj;
		if(otro.id.equals(id)) return true;
		
		boolean[] propiedades = {true,true};
		
		if(otro.nombre != null) {
			propiedades[0] = otro.nombre.equals(nombre);
		}
		if(otro.director != null) {
			propiedades[1] = otro.director.equals(director);
		}
		
		for(boolean propiedad : propiedades) {
			if(propiedad == false) return false;
		}
		return true;
	}
	
	//
}
