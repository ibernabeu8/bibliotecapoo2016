package gestorMateriales;

public class Revista extends Suscripcion{
	
	private String tematica;
	
	public Revista() {}
	//constructor por defecto, sin sucripcion
	public Revista(String nombre, String tematica) {
		super(nombre);
		this.setTematica(tematica);
	}
	//constructor con suscripcion
	public Revista(String nombre, String tematica, String periodo, double precio) {
		super(nombre,periodo,precio);
		this.setTematica(tematica);
	}
	@Override
	public String toString() {
		return "Revista, COD: " + id  + " Nombre: " + nombre + " Tematica: " + tematica + " Disponible: " + disponible;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null ) ? 0 : id.hashCode());
		result = prime * result + ((tematica == null) ? 0 : tematica.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		
		if(!(obj instanceof Revista)) {
			return false;
		}
		
		Revista otro = (Revista)obj;
		
		if(otro.id.equals(id)) return true;
		
		boolean[] propiedades = {true,true};
		
		if(otro.nombre != null) {
			propiedades[0] = otro.nombre.equals(nombre);
		}
		if(otro.tematica != null) {
			propiedades[1] = otro.tematica.equals(tematica);
		}
		
		for(boolean propiedad : propiedades) {
			if(propiedad == false) return false;
		}
		return true;
	}
	public String getTematica() {
		return tematica;
	}
	public void setTematica(String tematica) {
		this.tematica = tematica;
	}

	
	
}
