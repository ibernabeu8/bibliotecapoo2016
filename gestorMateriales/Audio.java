package gestorMateriales;

public class Audio extends Material{
	//propieades
	private String autor;
	//constructor
	public Audio() {}
	
	public Audio(String nombre,String autor) {
		super(autor);
	}
	//getsd && sets
	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	@Override
	public String toString() {
		return "Disco, COD: " + id + " Autor: " + autor + " Nombre: " + nombre + ", disponible= " + disponible ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((autor == null) ? 0 : autor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof Audio))
			return false;
		Audio otro = (Audio) obj;
		boolean[] propiedades = {true,true};
		if(otro.id.equals(id)) return true;
		
		if(otro.nombre != null) {
			propiedades[0] = otro.nombre.equals(nombre);
		}
		if(otro.autor != null) {
			propiedades[1] = otro.autor.equals(autor);
		}
		
		for(boolean propiedad : propiedades) {
			if(propiedad == false) return false;
		}
		return true;
	}

	
}
