package gestorMateriales;

public abstract class Suscripcion extends Material{
	
	//propiedades
	private double precio = 0;
	private String periodo = "No especificado";//en el que se suministra
	private boolean activa;
	
	//constructor
	public Suscripcion() {}
	public Suscripcion(String nombre) {
		super(nombre);
		this.activa = false;
	}
	public Suscripcion(String nombre, String periodo, double precio) {
		super(nombre);
		if(Double.valueOf(precio) != null) this.precio = precio;
		if(!(periodo.isEmpty() || periodo.equals(null))) setPeriodo(periodo);
	}

	public double getPrecio() {
		return precio;
	}

	public String getPeriodo() {
		return periodo;
	}

	public boolean isActiva() {
		return activa;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public void setPeriodo(String periodo) {
		periodo = periodo.toLowerCase().trim();
		switch(periodo) {
		case "semanal":
			break;
		case "mensual":
			break;
		case "anual":
			break;
		default:
			this.periodo = "No especificado";
			break;
		}
		if(this.periodo.equals(null)) this.periodo = periodo;
	}

	public void setActiva(boolean activa) {
		this.activa = activa;
	}
	public String toStringSuscripcion() {
		return "Suscripcion, COD: "+id +" Precio: " + precio + ", Periodo: " + periodo + ", activa: " + activa + ", nombre: " + nombre;
	}
	
}
