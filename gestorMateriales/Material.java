package gestorMateriales;

import java.io.Serializable;
import java.util.UUID;

public abstract class Material implements Serializable{
	
	//Propiedades
	protected String id;
	protected String nombre;
	protected boolean disponible;
	
	//Constructores
	public Material() {}
	
	public Material(String nombre) {
		this.id = UUID.randomUUID().toString();
		this.nombre = nombre;
		this.disponible = true;
	}
	//Getters and setters
	public String getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public boolean isDisponible() {
		return disponible;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}
	//Overrides, hashCode, equals y toString, declarandolos abstractos forzamos a que el resto de clases que hereden 
	//los sobrescriban.
	public abstract String toString();

	@Override
	public abstract int hashCode();
	@Override
	public abstract boolean equals(Object obj);
}
