package gestorMateriales;

public class Periodico extends Suscripcion{
	
	//propiedades
	private String fecha;

	//constructor
	public Periodico(){}
	public Periodico(String nombre, String fechaImpresion) {
		super(nombre);
		this.fecha = fechaImpresion;
	}
	public Periodico(String nombre, String fechaImpresion, String periodo, double precio){
		super(nombre,periodo,precio);
		this.setFecha(fechaImpresion);
	}
	
	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	@Override
	public String toString() {
		
		return "Periodico , Cod:" + id + " Fecha: "+ fecha + " Nombre: " + nombre + " Disponible: " + disponible;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		boolean[] propiedades = {true,true};

		if (!(obj instanceof Periodico))
			return false;
		
		Periodico otro = (Periodico) obj;
		if(otro.id.equals(id)) return true;
		
		if(otro.nombre != null) {
			propiedades[0] = otro.nombre.equals(nombre);
		}
		if(otro.fecha != null) {
			propiedades[1] = otro.fecha.equals(fecha);
		}
		
		for(boolean propiedad : propiedades) {
			if(propiedad == false) return false;
		}
		return true;
	}

}
