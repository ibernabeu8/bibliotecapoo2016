package gestorMateriales;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class GestorMateriales {
	//================================ATRIBUTOS=======================================
	public final static int TIPO_LIBRO = 1;
	public final static int TIPO_REVISTA = 2;
	public final static int TIPO_PERIODICO = 3;
	public final static int TIPO_AUDIO = 4;
	public final static int TIPO_VIDEO = 5;
	public final static int TIPO_SUSCRIPCION = 6;
	//propiedades, no me gusta la solucion, pero se nos exige una coleccion para cada tipo de material
	private HashSet<Libro> listadoLibros;
	private HashSet<Revista> listadoRevistas;
	private HashSet<Periodico> listadoPeriodicos;
	private HashSet<Audio> listadoAudios;
	private HashSet<Video> listadoVideos;
	private HashSet<Suscripcion> listadoSuscripciones;
	//URLS DE LOS DIFERENTES ARCHIVOS
	private final String URL_LIBROS = "data/libros.dat";
	private final String URL_REVISTAS = "data/revistas.dat";
	private final String URL_PERIODICOS = "data/periodicos.dat";
	private final String URL_AUDIOS = "data/audios.dat";
	private final String URL_VIDEOS = "data/videos.dat";
	private final String URL_SUSCRIPCIONES = "data/suscripciones.dat";
	//Streams de escritura
	private ObjectOutputStream oos;
	private ObjectInputStream ois;

	//Constructor
	public GestorMateriales() {
		init();
	}
	//inicializa los listados y los carga en memoria
	private void init() {
		//comprobamos si existen o no las diferentes rutas, las crea si no existieran
		utilidades.GeneradorURL.crearRuta(URL_LIBROS);
		utilidades.GeneradorURL.crearRuta(URL_REVISTAS);
		utilidades.GeneradorURL.crearRuta(URL_PERIODICOS);
		utilidades.GeneradorURL.crearRuta(URL_AUDIOS);
		utilidades.GeneradorURL.crearRuta(URL_VIDEOS);
		utilidades.GeneradorURL.crearRuta(URL_SUSCRIPCIONES);
		//carga de los distintos materiales
		cargar(TIPO_LIBRO);
		cargar(TIPO_REVISTA);
		cargar(TIPO_PERIODICO);
		cargar(TIPO_AUDIO);
		cargar(TIPO_VIDEO);
		cargar(TIPO_SUSCRIPCION);
	}
	//metodos publicos para dar de alta los diferentes materiales
	public void altaMaterial(Material m) {
		if(m instanceof Libro) {
			Libro l = (Libro)m;
			this.listadoLibros.add(l);
			guardar(TIPO_LIBRO);
		}
		else if(m instanceof Revista) {
			Revista r = (Revista)m;
			this.listadoRevistas.add(r);
			guardar(TIPO_REVISTA);
		}
		else if(m instanceof Periodico) {
			Periodico p = (Periodico)m;
			this.listadoPeriodicos.add(p);
			guardar(TIPO_PERIODICO);
		}
		else if(m instanceof Audio) {
			Audio a = (Audio)m;
			this.listadoAudios.add(a);
			guardar(TIPO_AUDIO);
		}
		else if(m instanceof Video) {
			Video v = (Video)m;
			this.listadoVideos.add(v);
			guardar(TIPO_VIDEO);
		}
	}
	//alta suscripcion
	public void altaSuscripcion(Suscripcion s){
		this.listadoSuscripciones.add(s);
		guardar(TIPO_SUSCRIPCION);
	}
	//bja suscripcion
	public void borrarSuscripcion(String id) {
		Iterator<Suscripcion> it = this.listadoSuscripciones.iterator();
		
		if(it.hasNext()) {
			Suscripcion s = it.next();
			if(s.id.equals(id)) this.listadoSuscripciones.remove(s);
			guardar(TIPO_SUSCRIPCION);
		}
			
	}
	//borrado
	public void borrar(String id) {
		Material m = busquedaSimple(id);
		
		if(m instanceof Libro) {
			this.listadoLibros.remove((Libro)m);
			guardar(TIPO_LIBRO);
		}
		else if(m instanceof Revista){ 
			this.listadoRevistas.remove((Revista)m);
			guardar(TIPO_REVISTA);
		}
		else if(m instanceof Periodico){ 
			this.listadoPeriodicos.remove((Periodico)m);
			guardar(TIPO_PERIODICO);
		}
		else if(m instanceof Audio) {
			this.listadoAudios.remove((Audio)m);
			guardar(TIPO_AUDIO);
		}
		else if(m instanceof Video){
			this.listadoVideos.remove((Video)m);
			guardar(TIPO_VIDEO);
		}
		
		
			
	}
	
	//metodos busqueda
	//busqueda simple
	public Material busquedaSimple(String id) {
		ArrayList<Material> listadoTodos = new ArrayList<Material>();
		listadoTodos.addAll(listadoLibros);
		listadoTodos.addAll(listadoRevistas);
		listadoTodos.addAll(listadoPeriodicos);
		listadoTodos.addAll(listadoAudios);
		listadoTodos.addAll(listadoVideos);

		for(Material m : listadoTodos) {
			if(m.getId().equals(id))  {
				return m;
			}
		}
		
		return null;
	}
	//metodos busqueda flexible
	public ArrayList<Libro> buscarLibro(Libro l) {
		Iterator<Libro> it = this.getListadoLibros().iterator();
		ArrayList<Libro> busqueda = new ArrayList<Libro>();
		while(it.hasNext()) {
			Libro actual = it.next();
			//System.out.println(actual.toString());
			if(actual.equals(l))
				busqueda.add(actual);
		}
		return busqueda;
	}
	
	public ArrayList<Revista> buscarRevista(Revista r) {
		Iterator<Revista> it = this.getListadoRevistas().iterator();
		ArrayList<Revista> busqueda = new ArrayList<Revista>();
		while(it.hasNext()) {
			Revista actual = it.next();
			//System.out.println(actual.toString());
			if(actual.equals(r))
				busqueda.add(actual);
		}
		return busqueda;
	}
	
	public ArrayList<Periodico> buscarPeriodico(Periodico p) {
		Iterator<Periodico> it = this.getListadoPeriodico().iterator();
		ArrayList<Periodico> busqueda = new ArrayList<Periodico>();
		while(it.hasNext()) {
			Periodico actual = it.next();
			//System.out.println(actual.toString());
			if(actual.equals(p))
				busqueda.add(actual);
		}
		return busqueda;
	}

	public ArrayList<Video> buscarVideo(Video v) {
		Iterator<Video> it = this.getListadoVideo().iterator();
		ArrayList<Video> busqueda = new ArrayList<Video>();
		while(it.hasNext()) {
			Video actual = it.next();
			//System.out.println(actual.toString());
			if(actual.equals(v))
				busqueda.add(actual);
		}
		return busqueda;
	}
	
	public ArrayList<Audio> buscarAudio(Audio a) {
		Iterator<Audio> it = this.getListadoAudio().iterator();
		ArrayList<Audio> busqueda = new ArrayList<Audio>();
		while(it.hasNext()) {
			Audio actual = it.next();
			//System.out.println(actual.toString());
			if(actual.equals(a))
				busqueda.add(actual);
		}
		return busqueda;
	}

	//listar
	public void listar(int tipo) {
		HashSet<? extends Material> listado;
		Iterator<? extends Material> it = null;
		switch(tipo) {
		case TIPO_LIBRO:
			listado = this.getListadoLibros();
			it = listado.iterator();
			break;
		case TIPO_REVISTA:
			listado = this.getListadoRevistas();
			it = listado.iterator();
			break;
		case TIPO_PERIODICO:
			listado = this.getListadoPeriodico();
			it = listado.iterator();
			break;
		case TIPO_AUDIO:
			listado = this.getListadoAudio();
			it = listado.iterator();
			break;
		case TIPO_VIDEO:
			listado = this.getListadoVideo();
			it = listado.iterator();
			break;
		case TIPO_SUSCRIPCION:
			listado = this.getListadoSuscripciones();
			it = listado.iterator();
			break;
		}
		
		while(it.hasNext()) {			
			System.out.println(it.next().toString());
		}
	}
	//disponible
	public void cambiarDisponibilidad(Material m, boolean valor) {
		borrar(m.id);
		m.setDisponible(valor);
		altaMaterial(m);
	}
	//GESTS LISTADOS
	public HashSet<Libro> getListadoLibros() {
		return listadoLibros;
	}
	public HashSet<Revista> getListadoRevistas() {
		return listadoRevistas;
	}
	public HashSet<Periodico> getListadoPeriodico() {
		return listadoPeriodicos;
	}
	public HashSet<Audio> getListadoAudio() {
		return listadoAudios;
	}
	public HashSet<Video> getListadoVideo() {
		return listadoVideos;
	}
	public HashSet<Suscripcion> getListadoSuscripciones() {
		return listadoSuscripciones;
	}
	
	//guardar
	private void guardar(int tipo) {
		try {
			switch(tipo) {
			
			case TIPO_LIBRO:
				this.oos = new ObjectOutputStream(new FileOutputStream(this.URL_LIBROS));
				this.oos.writeObject(this.listadoLibros);
				break;
			case TIPO_REVISTA:
				this.oos = new ObjectOutputStream(new FileOutputStream(this.URL_REVISTAS));
				this.oos.writeObject(this.listadoRevistas);
				break;
			case TIPO_PERIODICO:
				this.oos = new ObjectOutputStream(new FileOutputStream(this.URL_PERIODICOS));
				this.oos.writeObject(this.listadoPeriodicos);
				break;
			case TIPO_AUDIO:
				this.oos = new ObjectOutputStream(new FileOutputStream(this.URL_AUDIOS));
				this.oos.writeObject(this.listadoAudios);
				break;
			case TIPO_VIDEO:
				this.oos = new ObjectOutputStream(new FileOutputStream(this.URL_VIDEOS));
				this.oos.writeObject(this.listadoVideos);
				break;
			case TIPO_SUSCRIPCION:
				this.oos = new ObjectOutputStream(new FileOutputStream(this.URL_SUSCRIPCIONES));
				this.oos.writeObject(this.listadoSuscripciones);
				break;
			}
			this.oos.close();
		}catch (EOFException e) {
			System.out.println("Final de archivo.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	//cargar
	private void cargar(int tipo) {
		try {
			switch(tipo) {
			
			case TIPO_LIBRO:
				this.listadoLibros = new HashSet<Libro>();
				this.ois = new ObjectInputStream(new FileInputStream(this.URL_LIBROS));
				this.listadoLibros = (HashSet<Libro>) this.ois.readObject();
				break;
			case TIPO_REVISTA:
				this.listadoRevistas = new HashSet<Revista>();
				this.ois = new ObjectInputStream(new FileInputStream(this.URL_REVISTAS));
				this.listadoRevistas = (HashSet<Revista>) this.ois.readObject();
				break;
			case TIPO_PERIODICO:
				this.listadoPeriodicos = new HashSet<Periodico>();
				this.ois = new ObjectInputStream(new FileInputStream(this.URL_PERIODICOS));
				this.listadoPeriodicos = (HashSet<Periodico>) this.ois.readObject();
				break;
			case TIPO_AUDIO:
				this.listadoAudios = new HashSet<Audio>();
				this.ois = new ObjectInputStream(new FileInputStream(this.URL_AUDIOS));
				this.listadoAudios = (HashSet<Audio>)this.ois.readObject();
				break;
			case TIPO_VIDEO:
				this.listadoVideos = new HashSet<Video>();
				this.ois = new ObjectInputStream(new FileInputStream(this.URL_VIDEOS));
				this.listadoVideos = (HashSet<Video>)this.ois.readObject();
				break;
			case TIPO_SUSCRIPCION:
				this.listadoSuscripciones = new HashSet<Suscripcion>();
				this.ois = new ObjectInputStream(new FileInputStream(this.URL_SUSCRIPCIONES));
				this.listadoSuscripciones = (HashSet<Suscripcion>)this.ois.readObject();
				break;
			}
			this.ois.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}catch (EOFException e) {
			System.out.println("Final de archivo.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
